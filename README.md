[![pipeline status](https://gitlab.com/xw263/individual4/badges/main/pipeline.svg)](https://gitlab.com/xw263/individual4/-/commits/main)
# Individual Project 4

## Purpose
The goal of the project is to create a rust AWS lmbda function, a dynamo db, and view the whole process. 
This project randomly generates a song for the day. The song of the day is stored on the AWS dynamo DB. Then, visualize the whole process. 

## Demo Video:
https://youtu.be/PYxY1efuz4Q


![Screenshot_2024-04-14_at_12.46.09_AM](/uploads/991445f8368969c458effb47983d8349/Screenshot_2024-04-14_at_12.46.09_AM.png)

![Screenshot_2024-04-14_at_12.38.52_AM](/uploads/e8eda49bc535bc34edab430cb4452360/Screenshot_2024-04-14_at_12.38.52_AM.png)

![Screenshot_2024-04-14_at_12.36.36_AM](/uploads/e0637f9768a5b7db542d8c7069ca6ac1/Screenshot_2024-04-14_at_12.36.36_AM.png)


## Steps
#### Local File
1. Edit local file `main.rs` to create the main functions of the project
2. Edit `Makefile` and `Cargo.toml` file to create dependency
3. Do `cargo lambda watch` to test it locally
4. Create a new terminal, run the `cargo lambda invoke --data-ascii "{ \"command\": \"get\"}"` from the `Makefile`
5. Edit local file `db.rs` to create a database using rust
6. Create local file `view.rs` to give the totla number of occurance for one of the songs to interact with AWS Dynamo DB

#### Deploy on AWS
7. Add `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` (get it from AWS IAM) in `.env` in the repo. Add the same variables in the gitlab CI/CD variables. Add `.env` to `.gitignore`
8. Check if the function has been added to the AWS lambda function
9. In the AWS lambda, add roles. Click the link of the role name. 
10. Click `Add the permissions`, add `AmazonDynamoDBFullAccess` and `AWSLambdaBasicExecutionRole`
11. Test on AWS Lambda function if the generated outcome can be stored into AWS Dynamo DB tables
12. Create a new AWS Step functions. 
13. Add all 3 functions in order. Test it by only entering the command for the first function. 

## References
1. https://gitlab.com/jeremymtan/jeremytan_ids721_individual4
2. https://gitlab.com/xw263/mini_project_5
